package com.epam;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class App {
    private static Logger logger = LogManager.getLogger(App.class);
    public static void main(String[] args) {
        StringContainer c = new StringContainer();
        c.add("1");
        c.add("2");
        c.add("345");
        c.add("6");
        c.add("7");
        c.add("8");
        c.add("9");
        c.add("10");
        c.add("it is my contai-ner");
        logger.trace(c);

        logger.trace("After cleaning: ");
        c.clear();
        logger.trace(c);

        logger.trace("After removing 2:");
        c.add("1");
        c.add("2");
        c.add("345");
        c.remove("2");
        c.remove("5");
        logger.trace(c);

        logger.trace("After get 0:");
        logger.trace(c.get(0));


    }
}