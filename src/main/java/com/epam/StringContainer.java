package com.epam;

import java.lang.reflect.Array;
import java.util.*;

public class StringContainer  {
    String[] container;
    int count = 0;

    public StringContainer() {
        container = new String[10];
    }

    public void add(String str) {
        int oldCapasity = container.length;
        int newCapacity;

        if (isEmpty()==-1){
            newCapacity = oldCapasity + 1 ;
            this.container = Arrays.copyOf(this.container, newCapacity);
        }
        container[isEmpty()] = str;
        count++;
    }

//
    public int size() {
        return container.length;
    }
//
    private int isEmpty() {
        for (int i = 0; i < container.length; i++) {
             if(container[i] == null){
                 return i;
             }
        }
        return -1;
    }

//
    public boolean contains(String o) {
        for (String e: container) {
            if (e == o){
                return true;
            }
        }
        return false;
    }


//
    public boolean remove(String str) {
        try {
            container[find(str)] = null;
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public String [] getArray(){
        return container;
    }
//
    public void clear() {
        for (int i = 0; i< container.length; i++){
            container[i] = null;
        }
        container = new String[10];
        count = 0;
    }


    //
    public String get(int i){
        return container[i];
    }



    private int find(String str){
        for (int i = 0; i< container.length; i++){
            if(container[i]==str)
                return i;
        }
        return -1;
    }



    @Override
    public String toString() {
        return "StringContainer{" +
                "container=" + Arrays.toString(container) +
                '}';
    }

}
